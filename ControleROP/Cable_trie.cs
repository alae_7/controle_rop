﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleROP
{
    public class Cable_trie
    {
        public String Code_Cb { get; set; }
        public int Nombre_fibres { get; set; }
        public String Longueur { get; set; }
        public List<Tube> Tubes { get; set; }
        public String Cable_pere { get; set; }
        public int Tube_pere { get; set; }
    }
}