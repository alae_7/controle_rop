﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleROP
{
    public abstract class Utilitaires
    {
        //Style du fichier de sortie
        public static void Style_check_1(ExcelWorksheet ws)
        {
            //Zoom de la page
            ws.View.ZoomScale = 85;
            ws.Cells.AutoFitColumns();
            //Largeur des colonnes
            ws.Column(1).Width = 17;
            for (int j = 2; j <= 13; j++)
            {
                ws.Column(j).Width = 14;
            }
            ws.Column(8).Width = 41;
            ws.Column(15).Width = 20;
            ws.Column(16).Width = 29;
            //Lignes en gras
            ws.Row(1).Style.Font.Bold = true;
            ws.Row(2).Style.Font.Bold = true;
            //Couleurs des cellules
            ws.Cells[1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            ws.Cells[1, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 8].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            ws.Cells[1, 15].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 15].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            //Fusion
            ws.Cells[1, 1, 1, 6].Merge = true;
            ws.Cells[1, 8, 1, 13].Merge = true;
            ws.Cells[1, 15, 1, 16].Merge = true;
            //Centrer
            ws.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
        public static void Style_check_2(ExcelWorksheet ws)
        {
            //Zoom de la page
            ws.View.ZoomScale = 85;
            ws.Cells.AutoFitColumns();
            //Largeur des colonnes
            ws.Column(1).Width = 17;
            ws.Column(2).Width = 17;
            ws.Column(3).Width = 14;
            ws.Column(4).Width = 14;
            ws.Column(5).Width = 41;
            ws.Column(6).Width = 42;
            ws.Column(7).Width = 17;
            ws.Column(8).Width = 14;
            ws.Column(9).Width = 42;
            ws.Column(10).Width = 14;

            //Lignes en gras
            ws.Row(1).Style.Font.Bold = true;
            ws.Row(2).Style.Font.Bold = true;
            //Couleurs des cellules
            ws.Cells[1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            ws.Cells[1, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 6].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            //Fusion
            ws.Cells[1, 1, 1, 4].Merge = true;
            ws.Cells[1, 6, 1, 11].Merge = true;
            //Centrer
            ws.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        }
    }
}