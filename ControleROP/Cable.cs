﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleROP
{
    public class Cable
    {
        public String Code_Cb { get; set; }
        public String Bp_Aval { get; set; }
        public String Long_reel { get; set; }
        public int Ligne_excel { get; set; }
        public int Colonne_excel { get; set; }
        public int Tube { get; set; }
        public int Fibre { get; set; }
        public String Lien { get; set; }
        public String Cable_pere { get; set; }
        public int Tube_pere { get; set; }
    }
}
