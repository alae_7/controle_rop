﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleROP
{
    class Program
    {
        static void Main(string[] args)
        {
            String userText;
            String fi_rop_grace = "";
            String fi_rop_syno = "";
            Console.WriteLine("----------------------------------------------------------------------------------------");
            Console.WriteLine("---------------------------------------ATTENTION----------------------------------------");
            Console.WriteLine("----------------------------------------------------------------------------------------");
            Console.WriteLine("1-Le fichier Route optique issue du format GRACE doit se nommer : ROP GRACE \n");
            Console.WriteLine("2-Le fichier Route optique issue du format SYNOPTIQUE doit se nommer : ROP SYNO \n");
            Console.WriteLine("----------------------------------------------------------------------------------------");
            Console.WriteLine("----------------------------------------------------------------------------------------");
            Console.WriteLine("Veuillez coller le chemin de votre dossier ROP : \n");
            userText = Console.ReadLine();
            //userText = @"C:\Users\alae.ben.salah\Documents\_Megalis\ROP";
            DirectoryInfo dossier = new DirectoryInfo(userText);
            //Recup ROP SYNO
            foreach (var file in dossier.GetFiles("*.xlsx"))
            {
                if (file.Name.Contains("SYNO") && file.Name.Contains("ROP"))
                {
                    fi_rop_syno = file.FullName;
                }
            }
            //Recup ROP GRACE
            foreach (var file in dossier.GetFiles("*.xlsx"))
            {
                if (file.Name.Contains("GRACE") && file.Name.Contains("ROP"))
                {
                    fi_rop_grace = file.FullName;
                }
            }
            if (File.Exists(fi_rop_syno) && File.Exists(fi_rop_grace))
            {
                Console.WriteLine("Traitement en cours...");
                List<Cable> cb_rop_syno = new List<Cable>();
                List<Cable> cb_rop_grace = new List<Cable>();
                //Récup rop syno
                using (ExcelPackage packageExcel = new ExcelPackage(new FileInfo(fi_rop_syno)))
                {
                    foreach (ExcelWorksheet ongletExcel in packageExcel.Workbook.Worksheets)
                    {
                        if (ongletExcel.Name == "ROP")
                        {
                            //Ici on récupère le nombre de lignes et de colonnes 
                            int nbreLignes = ongletExcel.Dimension.End.Row;
                            int nbreColonnes = ongletExcel.Dimension.End.Column;
                            for (int j = 13; j <= nbreColonnes; j= j + 8)
                            {
                                for (int i = 2; i <= nbreLignes; i++)
                                {
                                    Cable c = new Cable();
                                    //Code cable
                                    if (ongletExcel.Cells[i, j].Value != null)
                                    {
                                        if (ongletExcel.Cells[i, j].Value.ToString().Trim() != "0")
                                        {
                                            //Si couleur différent de l'Orange et pas de câble D1
                                            //if (!(ongletExcel.Cells[i, j].Style.Fill.BackgroundColor.Rgb == "FFFFA500" && j != 13))
                                            //{
                                            c.Code_Cb = ongletExcel.Cells[i, j].Value.ToString().Trim();
                                            //Cable père
                                            if (ongletExcel.Cells[i, j].Offset(0, -8).Value != null && j != 13)
                                            {
                                                c.Cable_pere = ongletExcel.Cells[i, j].Offset(0, -8).Value.ToString().Trim();
                                                //Tube père
                                                if (ongletExcel.Cells[i, j].Offset(0, -6).Value != null)
                                                {
                                                    int tube_pere;
                                                    bool conversionOk = int.TryParse(ongletExcel.Cells[i, j].Offset(0, -6).Value.ToString().Trim(), out tube_pere);
                                                    if (conversionOk)
                                                    {
                                                        c.Tube_pere = tube_pere;
                                                    }
                                                    else
                                                    {
                                                        string e = "Impossible de récupérer le tube du câble père: " + c.Cable_pere + ", / ligne : " + i + " de la ROP SYNO ";
                                                        Console.WriteLine(e);
                                                    }
                                                }
                                            }
                                            //Tube
                                            if (ongletExcel.Cells[i, j + 2].Value != null)
                                            {
                                                int tube_solo;
                                                bool conversionOk = int.TryParse(ongletExcel.Cells[i, j + 2].Value.ToString().Trim(), out tube_solo);
                                                if (conversionOk)
                                                {
                                                    c.Tube = tube_solo;
                                                }
                                                else
                                                {
                                                    string e = "Impossible de récupérer le tube du câble : " + c.Code_Cb + ", / ligne : " + i + " de la ROP SYNO ";
                                                    Console.WriteLine(e);
                                                }
                                            }
                                            //Fibre
                                            if (ongletExcel.Cells[i, j + 3].Value != null)
                                            {
                                                int fibre_solo;
                                                bool conversionOk = int.TryParse(ongletExcel.Cells[i, j + 3].Value.ToString().Trim(), out fibre_solo);
                                                if (conversionOk)
                                                {
                                                    c.Fibre = fibre_solo;
                                                }
                                                else
                                                {
                                                    string e = "Impossible de récupérer le tube du câble : " + c.Code_Cb + ", / ligne : " + i + " de la ROP SYNO ";
                                                    Console.WriteLine(e);
                                                }
                                            }
                                            //Bp aval
                                            if (ongletExcel.Cells[i, j + 5].Value != null)
                                            {
                                                c.Bp_Aval = ongletExcel.Cells[i, j + 5].Value.ToString().Trim();
                                            }
                                            //Lien
                                            if (ongletExcel.Cells[i, j + 6].Value != null)
                                            {
                                                c.Lien = ongletExcel.Cells[i, j + 6].Value.ToString().Trim();
                                            }
                                            //Longueur
                                            if (ongletExcel.Cells[i, j + 7].Value != null)
                                            {
                                                c.Long_reel = ongletExcel.Cells[i, j + 7].Value.ToString().Trim();
                                            }
                                            c.Ligne_excel = i;
                                            c.Colonne_excel = j;
                                            cb_rop_syno.Add(c);
                                            //}                                         
                                        }                                        
                                    }
                                }
                            }                          
                        }
                    }
                }
                //Récup rop grace
                using (ExcelPackage packageExcel = new ExcelPackage(new FileInfo(fi_rop_grace)))
                {
                    foreach (ExcelWorksheet ongletExcel in packageExcel.Workbook.Worksheets)
                    {
                        if (ongletExcel.Name == "Route_Optique")
                        {
                            //Ici on récupère le nombre de lignes et de colonnes 
                            int nbreLignes = ongletExcel.Dimension.End.Row;
                            int nbreColonnes = ongletExcel.Dimension.End.Column;
                            for (int j = 15; j <= nbreColonnes; j= j + 8)
                            {
                                for (int i = 2; i <= nbreLignes; i++)
                                {
                                    Cable c = new Cable();
                                    //Code cable
                                    if (ongletExcel.Cells[i, j].Value != null)
                                    {
                                        if (ongletExcel.Cells[i, j].Value.ToString().Trim() != "0")
                                        {
                                            c.Code_Cb = ongletExcel.Cells[i, j].Value.ToString().Trim();
                                            //Cable père
                                            if (ongletExcel.Cells[i, j].Offset(0, -8).Value != null && j != 15)
                                            {
                                                c.Cable_pere = ongletExcel.Cells[i, j].Offset(0, -8).Value.ToString().Trim();
                                                //Tube père
                                                if (ongletExcel.Cells[i, j].Offset(0, -6).Value != null)
                                                {
                                                    int tube_pere;
                                                    bool conversionOk = int.TryParse(ongletExcel.Cells[i, j].Offset(0, -6).Value.ToString().Trim(), out tube_pere);
                                                    if (conversionOk)
                                                    {
                                                        c.Tube_pere = tube_pere;
                                                    }
                                                    else
                                                    {
                                                        string e = "Impossible de récupérer le tube du câble père: " + c.Cable_pere + ", / ligne : " + i + " de la ROP SYNO ";
                                                        Console.WriteLine(e);
                                                    }
                                                }
                                            }
                                            //Tube
                                            if (ongletExcel.Cells[i, j + 2].Value != null)
                                            {
                                                int tube_solo;
                                                bool conversionOk = int.TryParse(ongletExcel.Cells[i, j + 2].Value.ToString().Trim(), out tube_solo);
                                                if (conversionOk)
                                                {
                                                    c.Tube = tube_solo;
                                                }
                                                else
                                                {
                                                    string e = "Impossible de récupérer le tube du câble : " + c.Code_Cb + ", / ligne : " + i + " de la ROP SYNO ";
                                                    Console.WriteLine(e);
                                                }
                                            }
                                            //Fibre
                                            if (ongletExcel.Cells[i, j + 3].Value != null)
                                            {
                                                int fibre_solo;
                                                bool conversionOk = int.TryParse(ongletExcel.Cells[i, j + 3].Value.ToString().Trim(), out fibre_solo);
                                                if (conversionOk)
                                                {
                                                    c.Fibre = fibre_solo;
                                                }
                                                else
                                                {
                                                    string e = "Impossible de récupérer le tube du câble : " + c.Code_Cb + ", / ligne : " + i + " de la ROP SYNO ";
                                                    Console.WriteLine(e);
                                                }
                                            }
                                            //Bp aval
                                            if (ongletExcel.Cells[i, j + 5].Value != null)
                                            {
                                                c.Bp_Aval = ongletExcel.Cells[i, j + 5].Value.ToString().Trim();
                                            }
                                            //Lien
                                            if (ongletExcel.Cells[i, j + 6].Value != null)
                                            {
                                                c.Lien = ongletExcel.Cells[i, j + 6].Value.ToString().Trim();
                                            }
                                            //Longueur
                                            if (ongletExcel.Cells[i, j + 7].Value != null)
                                            {
                                                c.Long_reel = ongletExcel.Cells[i, j + 7].Value.ToString().Trim();
                                            }
                                            c.Ligne_excel = i;
                                            c.Colonne_excel = j;
                                            cb_rop_grace.Add(c);
                                        }                                        
                                    }
                                }
                            }                            
                        }
                    }
                }
                //Permet de trier et récupérer les câbles soudés pour une comparaison
                List<Cable_trie> cables_syno_tries = new List<Cable_trie>();
                List<Cable_trie> cables_grace_tries = new List<Cable_trie>();
                if (cb_rop_syno.Count != 0)
                {
                    //Récup des câbles de la ROP SYNO
                    //Cable trie est une classe qui contient 1 câble déployé et une liste de tubes
                    Cable_trie cb_trie = new Cable_trie();
                    cb_trie.Tubes = new List<Tube>();
                    //Permet de boucler sur la liste des câbles et récupérer 1 câble unique et sa liste de tubes associés 
                    foreach (Cable cb_syno in cb_rop_syno)
                    {          
                        //Si câble pas existant dans la liste => le rajouter
                        if (!cables_syno_tries.Exists(x => x.Code_Cb.Equals(cb_syno.Code_Cb)))
                        {
                            cb_trie = new Cable_trie();
                            cb_trie.Tubes = new List<Tube>();
                            Tube tube = new Tube();
                            //Code du câble
                            cb_trie.Code_Cb = cb_syno.Code_Cb;
                            //Numéro du tube
                            tube.Num_tube = cb_syno.Tube;
                            //Lien du tube
                            tube.Lien = cb_syno.Lien;
                            cb_trie.Tubes.Add(tube);
                            //Longueur du tube
                            cb_trie.Longueur = cb_syno.Long_reel;
                            //Cable Père
                            cb_trie.Cable_pere = cb_syno.Cable_pere;
                            //Tube Père
                            cb_trie.Tube_pere = cb_syno.Tube_pere;
                            cables_syno_tries.Add(cb_trie);
                        }
                        //Si le câble est déjà existant => rajouter que ses tubes associés
                        else if (cables_syno_tries.Exists(x=>x.Code_Cb.Equals(cb_syno.Code_Cb)))
                        {                            
                            Tube tube = new Tube();
                            tube.Num_tube = cb_syno.Tube;
                            tube.Lien = cb_syno.Lien;
                            //Si le tube n'est pas existant dans la liste des tubes => le rajouter
                            if (!cb_trie.Tubes.Exists(y=>y.Num_tube.Equals(tube.Num_tube)))
                            {
                                cb_trie.Tubes.Add(tube);
                            }
                            //Nombre de fibre utilisés (utiles)
                            cb_trie.Nombre_fibres = (cb_trie.Tubes.Count * 6);
                        }                   
                    }
                }
                if (cb_rop_grace.Count != 0)
                {
                    //Récup des câbles de la ROP SYNO
                    ////Cable trie est une classe qui contient 1 câble déployé et une liste de tubes
                    Cable_trie cb_trie = new Cable_trie();
                    cb_trie.Tubes = new List<Tube>();
                    foreach (Cable cb_grace in cb_rop_grace)
                    {
                        if (!cables_grace_tries.Exists(x => x.Code_Cb.Equals(cb_grace.Code_Cb)))
                        {
                            cb_trie = new Cable_trie();
                            cb_trie.Tubes = new List<Tube>();
                            Tube tube = new Tube();
                            //Récup code du câble
                            cb_trie.Code_Cb = cb_grace.Code_Cb;
                            //Récup num du tube
                            tube.Num_tube = cb_grace.Tube;
                            //Lien du tube (Attente, epissure...)
                            tube.Lien = cb_grace.Lien;
                            cb_trie.Tubes.Add(tube);
                            //Longueur du câble
                            cb_trie.Longueur = cb_grace.Long_reel;
                            //Cable Père
                            cb_trie.Cable_pere = cb_grace.Cable_pere;
                            //Tube Père
                            cb_trie.Tube_pere = cb_grace.Tube_pere;
                            cables_grace_tries.Add(cb_trie);
                        }
                        else if (cables_grace_tries.Exists(x => x.Code_Cb.Equals(cb_grace.Code_Cb)))
                        {
                            Tube tube = new Tube();                           
                            tube.Num_tube = cb_grace.Tube;
                            tube.Lien = cb_grace.Lien;
                            //Si tube pas existent => le rajouter
                            if (!cb_trie.Tubes.Exists(y => y.Num_tube.Equals(tube.Num_tube)))
                            {
                                cb_trie.Tubes.Add(tube);
                            }
                            //Nombre de fibre utilisés (utiles)
                            cb_trie.Nombre_fibres = (cb_trie.Tubes.Count * 6);
                        }
                    }
                }
                //Comparer les deux listes
                if (cables_syno_tries.Count != 0 && cables_grace_tries.Count != 0)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage())
                    {
                        //Propriétés du fichier EXCEL
                        excelPackage.Workbook.Properties.Author = "BE SNEF";
                        excelPackage.Workbook.Properties.Title = "Contrôle ROP Megalis";
                        excelPackage.Workbook.Properties.Created = DateTime.Now;
                        //Création du fichier Excel
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Check_1");
                        worksheet.Cells[1, 1].Value = "ROP Syno";
                        worksheet.Cells[2, 1].Value = "Code";
                        worksheet.Cells[2, 2].Value = "Nb tubes";
                        worksheet.Cells[2, 3].Value = "Fibres utilisées";
                        worksheet.Cells[2, 4].Value = "Epissure";
                        worksheet.Cells[2, 5].Value = "Attente";
                        worksheet.Cells[2, 6].Value = "Passage";
                        worksheet.Cells[1, 8].Value = "ROP Grace";
                        worksheet.Cells[2, 8].Value = "Code";
                        worksheet.Cells[2, 9].Value = "Nb tubes";
                        worksheet.Cells[2, 10].Value = "Fibres utilisées";
                        worksheet.Cells[2, 11].Value = "Epissure";
                        worksheet.Cells[2, 12].Value = "Attente";
                        worksheet.Cells[2, 13].Value = "Passage";
                        //Affichage des câbles
                        worksheet.Cells[1, 15].Value = "Tous les câbles GRACE";
                        worksheet.Cells[2, 15].Value = "Code";
                        worksheet.Cells[2, 16].Value = "Etat";
                        int i = 3;
                        foreach (Cable_trie c in cables_syno_tries)
                        {
                            int nb_tubes_epissure = 0;
                            int nb_tube_passage = 0;
                            int nb_tube_attente = 0;
                            worksheet.Cells[i, 1].Value = c.Code_Cb;
                            worksheet.Cells[i, 2].Value = c.Tubes.Count;
                            worksheet.Cells[i, 3].Value = c.Nombre_fibres;
                            //Tubes epissurés
                            nb_tubes_epissure = c.Tubes.Count(a => a.Lien.Equals("EPISSURE"));
                            worksheet.Cells[i, 4].Value = nb_tubes_epissure;
                            //Tubes en attente
                            nb_tube_attente = c.Tubes.Count(a => a.Lien.Equals("ATTENTE"));
                            worksheet.Cells[i, 5].Value = nb_tube_attente;
                            //Tubes en passage
                            nb_tube_passage = c.Tubes.Count(a => a.Lien.Equals("PASSAGE"));
                            worksheet.Cells[i, 6].Value = nb_tube_passage;
                            //--------------------------------------------------------------------------------------------
                            //Tester l'exisence dans la rop grace
                            if (cables_grace_tries.Exists(a=>a.Code_Cb.Equals(c.Code_Cb)))
                            {
                                Cable_trie cb_trouve = cables_grace_tries.Find(a => a.Code_Cb.Equals(c.Code_Cb));
                                worksheet.Cells[i, 8].Value = cb_trouve.Code_Cb;
                                //Tubes
                                if (cb_trouve.Tubes.Count == c.Tubes.Count)
                                {
                                    //Tubes
                                    worksheet.Cells[i, 9].Value = cb_trouve.Tubes.Count;
                                    //Fibres
                                    worksheet.Cells[i, 10].Value = cb_trouve.Tubes.Count * 6;
                                }
                                //Pas le même nombre de tubes
                                else
                                {
                                    //Tubes
                                    worksheet.Cells[i, 9].Value = cb_trouve.Tubes.Count;
                                    worksheet.Cells[i, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[i, 9].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                    //Fibres
                                    worksheet.Cells[i, 10].Value = cb_trouve.Tubes.Count * 6;
                                    worksheet.Cells[i, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[i, 10].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                }
                                //Lien EPISSURE
                                if (cb_trouve.Tubes.Count(b=>b.Lien.Equals("EPISSURE")) == nb_tubes_epissure)
                                {
                                    worksheet.Cells[i, 11].Value = cb_trouve.Tubes.Count(b => b.Lien.Equals("EPISSURE"));
                                }
                                else
                                {
                                    worksheet.Cells[i, 11].Value = cb_trouve.Tubes.Count(b => b.Lien.Equals("EPISSURE"));
                                    worksheet.Cells[i, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[i, 11].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                }
                                //Lien ATTENTE
                                if (cb_trouve.Tubes.Count(b => b.Lien.Equals("ATTENTE")) == nb_tube_attente)
                                {
                                    worksheet.Cells[i, 12].Value = cb_trouve.Tubes.Count(b => b.Lien.Equals("ATTENTE"));
                                }
                                else
                                {
                                    worksheet.Cells[i, 12].Value = cb_trouve.Tubes.Count(b => b.Lien.Equals("ATTENTE"));
                                    worksheet.Cells[i, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[i, 12].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                }
                                //Lien PASSAGE
                                if (cb_trouve.Tubes.Count(b => b.Lien.Equals("PASSAGE")) == nb_tube_passage)
                                {
                                    worksheet.Cells[i, 13].Value = cb_trouve.Tubes.Count(b => b.Lien.Equals("PASSAGE"));
                                }
                                else
                                {
                                    worksheet.Cells[i, 13].Value = cb_trouve.Tubes.Count(b => b.Lien.Equals("PASSAGE"));
                                    worksheet.Cells[i, 13].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[i, 13].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                }
                            }
                            else
                            {
                                //Cable Inexistant dans la ROP Grace
                                worksheet.Cells[i, 8].Value = "Cable à vérifier (A DEPLOYER ou INEXISTANT)";
                                worksheet.Cells[i, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[i, 8].Style.Fill.BackgroundColor.SetColor(Color.Orange);
                            }
                            i++;
                        }
                        i = 3;
                        foreach (Cable_trie cb_trie in cables_grace_tries)
                        {
                            worksheet.Cells[i, 15].Value = cb_trie.Code_Cb;
                            if (cables_syno_tries.Exists(a => a.Code_Cb.Equals(cb_trie.Code_Cb)))                              
                            {
                                worksheet.Cells[i, 16].Value = "Trouvé";
                            }
                            else
                            {
                                worksheet.Cells[i, 16].Value = "Introuvable dans la ROP SYNO";
                                //worksheet.Cells[i, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[i, 16].Style.Font.Color.SetColor(Color.Red);
                            }
                            i++;
                        }
                        //Concerne le deuxième onglet pour le check de l'ordre en utilisant les tubes et câbles pères
                        ExcelWorksheet worksheet_2 = excelPackage.Workbook.Worksheets.Add("Check_2");
                        worksheet_2.Cells[1, 1].Value = "ROP Syno";
                        worksheet_2.Cells[2, 1].Value = "Code";
                        worksheet_2.Cells[2, 2].Value = "Câble père";
                        worksheet_2.Cells[2, 3].Value = "Tube dép-père";
                        worksheet_2.Cells[2, 4].Value = "Tube(s)";

                        worksheet_2.Cells[1, 6].Value = "ROP Grace";
                        worksheet_2.Cells[2, 6].Value = "Code";
                        worksheet_2.Cells[2, 7].Value = "Câble père";
                        worksheet_2.Cells[2, 8].Value = "Tube dép-père";
                        worksheet_2.Cells[2, 9].Value = "Remarque 1";
                        worksheet_2.Cells[2, 10].Value = "Tube(s)";
                        worksheet_2.Cells[2, 11].Value = "Remarque 2";

                        i = 3;
                        foreach (Cable_trie cb_syno_trie in cables_syno_tries)
                        {
                            worksheet_2.Cells[i, 1].Value = cb_syno_trie.Code_Cb;
                            worksheet_2.Cells[i, 2].Value = cb_syno_trie.Cable_pere;
                            worksheet_2.Cells[i, 3].Value = cb_syno_trie.Tube_pere;
                            //Existance dans ROP GRACE
                            if (cables_grace_tries.Exists(a => a.Code_Cb.Equals(cb_syno_trie.Code_Cb)))
                            {
                                Cable_trie cb_trouve = cables_grace_tries.Find(a => a.Code_Cb.Equals(cb_syno_trie.Code_Cb));
                                worksheet_2.Cells[i, 6].Value = cb_trouve.Code_Cb;
                                //Cable père grace = câble père syno
                                if (cb_trouve.Cable_pere == cb_syno_trie.Cable_pere)
                                {                                
                                    //Tube père trouvé dans grace = tube père rop syno
                                    if (cb_trouve.Tube_pere == cb_syno_trie.Tube_pere)
                                    {
                                        worksheet_2.Cells[i, 7].Value = cb_trouve.Cable_pere;
                                        worksheet_2.Cells[i, 8].Value = cb_trouve.Tube_pere;
                                        worksheet_2.Cells[i, 9].Value = "Ok";
                                    }
                                    else
                                    {
                                        worksheet_2.Cells[i, 7].Value = cb_trouve.Cable_pere;
                                        worksheet_2.Cells[i, 8].Value = cb_trouve.Tube_pere;
                                        worksheet_2.Cells[i, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        worksheet_2.Cells[i, 8].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                        worksheet_2.Cells[i, 9].Value = "Incohérence ordre de soudure pour ce câble";
                                        worksheet_2.Cells[i, 9].Style.Font.Color.SetColor(Color.Red);
                                    }
                                    //Comparaison des tubes courants
                                    //Tubes Syno
                                    string tubes_syno_txt = "";
                                    string tubes_grace_txt = "";
                                    tubes_syno_txt = string.Join(",", cb_syno_trie.Tubes.Select(r=>r.Num_tube));
                                    tubes_grace_txt = string.Join(",", cb_trouve.Tubes.Select(r => r.Num_tube));
                                    if (tubes_syno_txt == tubes_grace_txt)
                                    {
                                        worksheet_2.Cells[i, 4].Value = tubes_syno_txt;
                                        worksheet_2.Cells[i, 10].Value = tubes_grace_txt;
                                        worksheet_2.Cells[i, 11].Value = "Ok";
                                    }
                                    else
                                    {
                                        worksheet_2.Cells[i, 4].Value = tubes_syno_txt;
                                        worksheet_2.Cells[i, 10].Value = tubes_grace_txt;
                                        worksheet_2.Cells[i, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        worksheet_2.Cells[i, 10].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                        worksheet_2.Cells[i, 11].Value = "Incohérence tube(s) utilisé(s)";
                                        worksheet_2.Cells[i, 11].Style.Font.Color.SetColor(Color.Red);
                                    }                                    
                                }
                                //Cable père grace != câble père syno
                                else
                                {
                                    worksheet_2.Cells[i, 7].Value = cb_trouve.Tube_pere;
                                    worksheet_2.Cells[i, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet_2.Cells[i, 7].Style.Fill.BackgroundColor.SetColor(Color.Red);
                                    worksheet_2.Cells[i, 8].Value = 0;
                                    worksheet_2.Cells[i, 9].Value = "Incohérence soudure détecté - câble père différent";
                                    worksheet_2.Cells[i, 9].Style.Font.Color.SetColor(Color.Red);
                                    worksheet_2.Cells[i, 10].Value = 0;
                                }                          
                            }
                            else
                            {
                                //Cable Inexistant dans la ROP Grace
                                worksheet_2.Cells[i, 6].Value = "Cable à vérifier (A DEPLOYER ou INEXISTANT)";
                                worksheet_2.Cells[i, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet_2.Cells[i, 6].Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                worksheet_2.Cells[i, 9].Value = "A vérifier";
                                worksheet_2.Cells[i, 9].Style.Font.Color.SetColor(Color.OrangeRed);
                            }
                            worksheet_2.Cells[i, 5].Value = "-";
                            i++;
                        }
                        //Syle pour onglet Check
                        Utilitaires.Style_check_1(worksheet);
                        Utilitaires.Style_check_2(worksheet_2);
                        //save excel file
                        FileInfo fi = new FileInfo(userText + "\\" + "_Controle ROP_" + DateTime.Now.Date.ToString("dd-MM-yyyy") + ".xlsx");
                        excelPackage.SaveAs(fi);
                        Console.WriteLine("Traitement terminé ! \n");
                        Console.WriteLine("Cliquer sur une touche pour terminer.");
                        Console.ReadLine();
                    }
                }
            }
            else
            {
                Console.WriteLine("Vérifier bien l'existence des fichiers demandés ! \n");
                Console.WriteLine("Cliquer sur une touche pour terminer.");
                Console.ReadLine();
            }
        }
    }
}